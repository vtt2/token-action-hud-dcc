// System Module Imports
import {Utils} from './utils.js'
import {SystemManager} from "./system-manager.js";

export let ActionHandler = null

Hooks.once('tokenActionHudCoreApiReady', async (coreModule) => {
    /**
     * Extends Token Action HUD Core's ActionHandler class and builds system-defined actions for the HUD
     */



    ActionHandler = class ActionHandler extends coreModule.api.ActionHandler {
        /**
         * Build system actions
         * Called by Token Action HUD Core
         * @override
         * @param {array} groupIds
         */
        async buildSystemActions(groupIds) {

            this.groupHandler.updateGroup({id: 'abilities', settings: {style: 'list', showTitle: false}})
            this.groupHandler.updateGroup({id: 'abilities_abilities', settings: {style: 'list'}})
            this.groupHandler.updateGroup({id: 'abilities_modified', settings: {style: 'list'}})

            if (this.actor) {
                this.buildCharacterActions();
            } else if (!this.actor) {
                this.buildMultipleTokenActions();
            }
            this.buildUtilityActions();
        }

        buildMultipleTokenActions() {

        }


        disP(val) {
            if (val >= 0) {
                if (typeof val === 'string' && val.startsWith('+') ) { return val; }
                return `+${val}`;
            } else {
                return val;
            }
        }

        /**
         * Build character actions
         * @private
         */
        buildCharacterActions() {
            // Abilities
            const abilities = this.actor.system.abilities;
            const weapons = this.actor.items.filter(w => w.type === 'weapon');
            const saves = this.actor.system.saves;

            const saveActions = [];
            const abilityActions = [];
            const weaponActions = [];

            //const rollModifierDefault = game.settings.get('dcc', 'showRollModifierByDefault');

            let rollModifierDefault = false;
            let settingsCheck = setInterval(function () {
                if (game.settings.get('dcc', 'showRollModifierByDefault') !== undefined) {
                    clearInterval(settingsCheck);
                    rollModifierDefault = game.settings.get('dcc', 'showRollModifierByDefault');
                }
            }, 100);

            if (!rollModifierDefault) {
                for (let a in saves) {
                    const name = game.i18n.localize(saves[a].label);
                    const saveEncodedValue = ['saves', a].join('|');
                    const info1 = { text: `${this.disP(saves[a].value)}`};
                    saveActions.push({name, id: a, info1, encodedValue: saveEncodedValue});
                }
                this.addActions(saveActions, {id: 'saves', type: 'system'})
            }

            saveActions.length = 0;
            for (let a in this.actor.system.saves) {
                const name = game.i18n.localize(saves[a].label);
                const saveEncodedValue = ['saves_modified', a].join('|');
                const info1 = { text: `${this.disP(saves[a].value)}`};
                saveActions.push({name, id: a, info1, encodedValue: saveEncodedValue});
            }
            this.addActions(saveActions, {id: 'saves_modified', type: 'system'})

            if (!rollModifierDefault) {
                for (let a in abilities) {
                    const name = game.i18n.localize(abilities[a].label);
                    const attributeEncodedValue = ['ability', a].join('|');
                    const info1 = { text: `${abilities[a].value}(${this.disP(abilities[a].mod)})`};
                    abilityActions.push({name, id: a, info1, encodedValue: attributeEncodedValue});
                }
                this.addActions(abilityActions, {id: 'abilities', type: 'system'})
            }

            abilityActions.length = 0;
            for (let a in this.actor.system.abilities) {
                const name = game.i18n.localize(abilities[a].label);
                const attributeEncodedValue = ['ability_modified', a].join('|');
                const info1 = { text: `${abilities[a].value}(${this.disP(abilities[a].mod)})`};
                abilityActions.push({name, id: a, info1, encodedValue: attributeEncodedValue});
            }
            this.addActions(abilityActions, {id: 'abilities_modified', type: 'system'})

            if (!rollModifierDefault) {
                for (let a in weapons) {
                    const info1 = { text: String(weapons[a].system.attackBonus) };
                    const name = game.i18n.localize(weapons[a].name);
                    const weaponEncodedValue = ['weapons', weapons[a].id].join('|');
                    weaponActions.push({name: name, id: a, info1, encodedValue: weaponEncodedValue});
                }
            }
            this.addActions(weaponActions, {id: 'weapons', type: 'system'})

            weaponActions.length = 0;
            for (let a in weapons) {
                const info1 = { text: String(weapons[a].system.attackBonus) };
                const name = game.i18n.localize(weapons[a].name);
                const attributeEncodedValue = ['weapons_modified', weapons[a].id].join('|');
                weaponActions.push({name: name, id: a, info1, encodedValue: attributeEncodedValue});
            }
            this.addActions(weaponActions, {id: 'weapons_modified', type: 'system'})

            switch (this.actor.system.class.className) {
                case coreModule.api.Utils.i18n('DCC.Cleric'):
                    this.buildSpellActions(rollModifierDefault, coreModule.api.Utils.i18n('DCC.ClericSpells'));
                    break;
                case coreModule.api.Utils.i18n('DCC.Elf'):
                    this.buildSpellActions(rollModifierDefault, coreModule.api.Utils.i18n('DCC.ElfSpells'));
                    break;
                case coreModule.api.Utils.i18n('DCC.Wizard'):
                    this.buildSpellActions(rollModifierDefault, coreModule.api.Utils.i18n('DCC.WizardSpells'));
                    break;
                default:
                    break;
            }


            if (this.actor.type === 'Player') {
                const classUpdateData = {
                    id: 'class',
                    name: this.actor.system.class.className,
                    type: 'custom'
                }
                this.groupHandler.updateGroup(classUpdateData);

                const classGroupData = {
                    id: this.actor.system.class.className.toLowerCase(),
                    type: 'system',
                    name: this.actor.system.class.className
                }
                const classModifiedGroupData = {
                    id: this.actor.system.class.className.toLowerCase() + 'modified',
                    type: 'system',
                    name: this.actor.system.class.className + ' (Modified)'
                }
                const classParentGroupData = {
                    id: 'class',
                    nestId: 'class',
                    type: 'custom'
                }

                this.groupHandler.addGroup(classGroupData, classParentGroupData, true);
                this.groupHandler.addGroup(classModifiedGroupData, classParentGroupData, true);

                const CLERIC_LIST = {
                    spell_check: {
                        name: game.i18n.localize('DCC.SpellCheck'),
                        roll: 'spellCheck',
                        info1: { text: String(this.actor.system.class.spellCheck) }
                    },
                    divine_aid: {
                        name: game.i18n.localize('DCC.DivineAid'),
                        roll: 'skill_divineAid',
                        info1: { text: String(this.actor.system.class.spellCheck) }
                    },
                    turn_unholy: {
                        name: game.i18n.localize('DCC.TurnUnholy'),
                        roll: 'skill_turnUnholy',
                        info1: { text: String(this.actor.system.skills.turnUnholy.value) }
                    },
                    lay_on_hands: {
                        name: game.i18n.localize('DCC.LayOnHands'),
                        roll: 'skill_layOnHands',
                        info1: { text: String(this.actor.system.class.spellCheck) }
                    }
                }

                const DWARF_LIST = {
                }

                const ELF_LIST = {
                    spell_check: {
                        name: game.i18n.localize('DCC.SpellCheck'),
                        roll: 'spellCheck',
                        info1: { text: String(this.actor.system.class.spellCheck) }
                    },
                    find_secret_doors: {
                        name: game.i18n.localize('DCC.HeightenedSenses'),
                        roll: 'skill_detectSecretDoors',
                        info1: { text: String(this.actor.system.skills.detectSecretDoors?.value) }
                    }
                }

                const HALFLING_LIST = {
                    sneak_and_hide: {
                        name: game.i18n.localize('DCC.SneakAndHide'),
                        roll: 'skill_sneakAndHide',
                        info1: { text: String(this.actor.system.skills.sneakAndHide.value) }
                    }
                }

                const THIEF_LIST = {
                    luck_die: {
                        name: game.i18n.localize('DCC.LuckDie'),
                        roll: 'luck_die',
                        info1: { text: String(this.actor.system.class.luckDie) }
                    },
                    sneak_silently: {
                        name: game.i18n.localize('DCC.SneakSilently'),
                        roll: 'skill_sneakSilently',
                        info1: { text: String(this.disP(this.actor.system.skills.sneakSilently.value)) }
                    },
                    hide_in_shadows: {
                        name: game.i18n.localize('DCC.HideInShadows'),
                        roll: 'skill_hideInShadows',
                        info1: { text: String(this.disP(this.actor.system.skills.hideInShadows.value)) }
                    },
                    pick_pockets: {
                        name: game.i18n.localize('DCC.PickPocket'),
                        roll: 'skill_pickPockets',
                        info1: { text: String(this.disP(this.actor.system.skills.pickPockets.value)) }
                    },
                    pick_lock: {
                        name: game.i18n.localize('DCC.PickLock'),
                        roll: 'skill_pickLock',
                        info1: { text: String(this.disP(this.actor.system.skills.pickLock.value)) }
                    },
                    climb_sheer_surfaces: {
                        name: game.i18n.localize('DCC.ClimbSheerSurfaces'),
                        roll: 'skill_climbSheerSurfaces',
                        info1: { text: String(this.disP(this.actor.system.skills.climbSheerSurfaces.value)) }
                    },
                    find_trap: {
                        name: game.i18n.localize('DCC.FindTrap'),
                        roll: 'skill_findTrap',
                        info1: { text: String(this.disP(this.actor.system.skills.findTrap.value)) }
                    },
                    disable_trap: {
                        name: game.i18n.localize('DCC.DisableTrap'),
                        roll: 'skill_disableTrap',
                        info1: { text: String(this.disP(this.actor.system.skills.disableTrap.value)) }
                    },
                    forge_document: {
                        name: game.i18n.localize('DCC.ForgeDocument'),
                        roll: 'skill_forgeDocument',
                        info1: { text: String(this.disP(this.actor.system.skills.forgeDocument.value)) }
                    },
                    disguise_self: {
                        name: game.i18n.localize('DCC.DisguiseSelf'),
                        roll: 'skill_disguiseSelf',
                        info1: { text: String(this.disP(this.actor.system.skills.disguiseSelf.value)) }
                    },
                    read_languages: {
                        name: game.i18n.localize('DCC.ReadLanguages'),
                        roll: 'skill_readLanguages',
                        info1: { text: String(this.disP(this.actor.system.skills.readLanguages.value)) }
                    },
                    handle_poison: {
                        name: game.i18n.localize('DCC.HandlePoison'),
                        roll: 'skill_handlePoison',
                        info1: { text: String(this.disP(this.actor.system.skills.handlePoison.value)) }
                    },
                    cast_spell_from_scroll: {
                        name: game.i18n.localize('DCC.CastSpellFromScroll'),
                        roll: 'skill_castSpellFromScroll',
                        info1: { text: String(this.disP(this.actor.system.skills.castSpellFromScroll.value)) }
                    }
                }

                const WARRIOR_LIST = {
                }

                const WIZARD_LIST = {
                    spell_check: {
                        name: game.i18n.localize('DCC.SpellCheck'),
                        roll: 'spellCheck',
                        info1: { text: String(this.actor.system.class.spellCheck) }
                    }

                }

                let classList = {};
                switch (this.actor.system.class.className) {
                    case coreModule.api.Utils.i18n('DCC.Cleric'):
                        classList = CLERIC_LIST;
                        break;
                    case coreModule.api.Utils.i18n('DCC.Dwarf'):
                        classList = DWARF_LIST;
                        break;
                    case coreModule.api.Utils.i18n('DCC.Elf'):
                        classList = ELF_LIST;
                        break;
                    case coreModule.api.Utils.i18n('DCC.Halfling'):
                        classList = HALFLING_LIST;
                        break;
                    case coreModule.api.Utils.i18n('DCC.Thief'):
                        classList = THIEF_LIST;
                        break;
                    case coreModule.api.Utils.i18n('DCC.Warrior'):
                        classList = WARRIOR_LIST;
                        break;
                    case coreModule.api.Utils.i18n('DCC.Wizard'):
                        classList = WIZARD_LIST;
                        break;
                    default:
                        break;
                }

                const classActions = [];
                if (!rollModifierDefault) {
                    for (const action in classList) {
                        if(!this.actor.system.config.attackBonusMode === 'manual' && action === 'deed') continue;
                        const actionEncodedValue = ['class', classList[action].roll].join('|');
                        classActions.push({
                            name: classList[action].name,
                            id: action,
                            info1: classList[action].info1,
                            info2: classList[action].info2,
                            encodedValue: actionEncodedValue});
                    }
                    this.addActions(classActions, {id: this.actor.system.class.className.toLowerCase(), type: 'system'})
                }
                classActions.length = 0;
                for (const action in classList) {
                    if(!(this.actor.system.config.attackBonusMode === 'manual') && action === 'deed') continue;
                    const actionEncodedValue = ['class_modified', classList[action].roll].join('|');
                    classActions.push({
                        name: classList[action].name,
                        id: action,
                        info1: classList[action].info1,
                        info2: classList[action].info2,
                        encodedValue: actionEncodedValue
                    });
                }
                this.addActions(classActions,
                    {id: this.actor.system.class.className.toLowerCase() + 'modified', type: 'system'})
            }
        }

        buildSpellActions(rollModifierDefault, spellType) {
            const updateData = {
                id: 'spells',
                name: spellType,
                type: 'custom'
            }
            this.groupHandler.updateGroup(updateData);
            const spellGroupData = {
                id: 'spells',
                type: 'system',
                name: spellType
            }
            const spellModifiedGroupData = {
                id: 'modified',
                type: 'system',
                name: spellType + ' (Modified)'
            }
            const parentGroupData = {
                id: 'spells',
                nestId: 'spells',
                type: 'custom'
            }
            this.addGroup(spellGroupData, parentGroupData, true);
            this.addGroup(spellModifiedGroupData, parentGroupData, true);

            const spells = this.actor.items.filter(w => w.type === 'spell').sort((a, b) => a.system.level > b.system);

            for (let a in spells) {
                const groupId = 'level' + spells[a].system.level;
                const groupName = coreModule.api.Utils.i18n('DCC.system.class.spellsLevel' + spells[a].system.level);
                const spellGroupData = {
                    id: groupId,
                    type: 'system',
                    name: groupName
                }
                const spellModifiedGroupData = {
                    id: groupId + 'modified',
                    type: 'system',
                    name: groupName + ' (Modified)'
                }
                const parentGroupData = {
                    id: 'spells',
                    type: 'system',
                    nestId: 'spells_spells'
                }
                const parentModifiedGroupData = {
                    id: 'modified',
                    type: 'system',
                    nestId: 'spells_modified'
                }
                this.addGroup(spellGroupData, parentGroupData, true);
                this.addGroup(spellModifiedGroupData, parentModifiedGroupData, true);
                const name = spells[a].name;
                const id = spells[a].id;
                if (!rollModifierDefault) {
                    let spellEncodedValue = ['spells', id].join('|');
                    let spellAction = {name: name, id: a, tooltip: {content: String(spells[a].system.description.value)},
                        info1: {text: this.actor.system.class.spellCheck}, encodedValue: spellEncodedValue};
                    this.addActions([spellAction], {id: groupId, type: 'system'});
                }
                let spellEncodedValue = ['spells_modified', id].join('|');
                let spellAction = {name: name, id: a, tooltip: {content: String(spells[a].system.description.value)},
                    info1: {text: this.actor.system.class.spellCheck}, encodedValue: spellEncodedValue};
                this.addActions([spellAction], {id: groupId + 'modified', type: 'system'})
            }

        }

        buildUtilityActions() {
            const utilityActions = [];

            const endTurnAction = {
                name: "End Turn",
                encodedValue: ['utility', 'endTurn'].join('|')
            }
            utilityActions.push(endTurnAction);
            this.addActions(utilityActions, {id: 'utility', type: 'system'})
        }

        /**
         * Build multiple token actions
         * @private
         * @returns {object}
         */
        #buildMultipleTokenActions() {
        }
    }
})
