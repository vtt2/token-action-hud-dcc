export let RollHandler = null

Hooks.once('tokenActionHudCoreApiReady', async (coreModule) => {
    /**
     * Extends Token Action HUD Core's RollHandler class and handles action events triggered when an action is clicked
     */
    RollHandler = class RollHandler extends coreModule.api.RollHandler {
        /**
         * Handle action event
         * Called by Token Action HUD Core when an action event is triggered
         * @override
         * @param {object} event        The event
         * @param {string} encodedValue The encoded value
         */
        async handleActionClick(event, encodedValue) {
            const payload = encodedValue.split('|')

            if (payload.length !== 2) {
                super.throwInvalidValueErr()
            }

            const actionTypeId = payload[0]
            const actionId = payload[1]

            const renderable = ['item']

            if (renderable.includes(actionTypeId) && this.isRenderItem()) {
                return this.doRenderItem(this.actor, actionId)
            }

            const knownCharacters = ['character']

            // If single actor is selected
            if (this.actor) {
                await this.#handleAction(event, this.actor, this.token, actionTypeId, actionId)
                return
            }

            const controlledTokens = canvas.tokens.controlled
                .filter((token) => knownCharacters.includes(token.actor?.type))

            // If multiple actors are selected
            for (const token of controlledTokens) {
                const actor = token.actor
                await this.#handleAction(event, actor, token, actionTypeId, actionId)
            }
        }

        /**
         * Handle action
         * @private
         * @param {object} event        The event
         * @param {object} actor        The actor
         * @param {object} token        The token
         * @param {string} actionTypeId The action type id
         * @param {string} actionId     The actionId
         */
        async #handleAction(event, actor, token, actionTypeId, actionId) {
            const options = {};
            switch (actionTypeId) {

                case 'weapons':
                   await this.#handleWeaponAction(event, actor, actionId);
                    break;

                case 'weapons_modified':
                    await this.#handleModifiedWeaponAction(event, actor, actionId);
                    break;

                case 'spells':
                    await this.#handleSpellAction(event, actor, actionId);
                    break;

                case 'spells_modified':
                    await this.#handleModifiedSpellAction(event, actor, actionId);
                    break;
                    
                case 'saves':
                    await this.#handleSaveAction(event, actor, actionId);
                    break;

                case 'saves_modified':
                    await this.#handleModifiedSaveAction(event, actor, actionId);
                    break;

                case 'ability':
                    this.#handleAbilityAction(event, actor, actionId);
                    break;

                case 'ability_modified':
                    this.#handleModifiedAbilityAction(event, actor, actionId);
                    break;

                case 'class':
                    this.#handleClassAction(event, actor, actionId);
                    break;

                case 'class_modified':
                    this.#handleClassModifiedAction(event, actor, actionId);
                    break;

                case 'utility':
                    this.#handleUtilityAction(token, actionId);
                    break;
            }
        }

        /**
         * Handle weapon action
         * @private
         * @param {object} event    The event
         * @param {object} actor    The actor
         * @param {string} actionId The action id
         */
        async #handleWeaponAction(event, actor, actionId) {
            if (event.ctrlKey || event.metaKey)  {
                await this.#handleModifiedWeaponAction(event, actor, actionId)
            } else {
                await actor.rollWeaponAttack(actionId, {})
            }
        }

        /**
         * Handle modified weapon action
         * @private
         * @param {object} event    The event
         * @param {object} actor    The actor
         * @param {string} actionId The action id
         */
        async #handleModifiedWeaponAction(event, actor, actionId) {
            await actor.rollWeaponAttack(actionId, {showModifierDialog: true})
        }


        /**
         * Handle spell action
         * @private
         * @param {object} event    The event
         * @param {object} actor    The actor
         * @param {string} actionId The action id
         */
        async #handleSpellAction(event, actor, actionId) {
            if (event.ctrlKey || event.metaKey)  {
                await this.#handleModifiedSpellAction(event, actor, actionId)
            } else {
                const options = {};
                const item = actor.items.find(i=>i.id === actionId);
                await item.rollSpellCheck(actor.system.class.spellCheckAbility,options);
            }
        }

        /**
         * Handle modified spell action
         * @private
         * @param {object} event    The event
         * @param {object} actor    The actor
         * @param {string} actionId The action id
         */
        async #handleModifiedSpellAction(event, actor, actionId) {
            const options = {};
            const item = actor.items.find(i=>i.id === actionId);
            options.showModifierDialog = true;
            await item.rollSpellCheck(actor.system.class.spellCheckAbility,options);
        }
        
        /**
         * Handle save action
         * @private
         * @param {object} event    The event
         * @param {object} actor    The actor
         * @param {string} actionId The action id
         */
        async #handleSaveAction(event, actor, actionId) {
            if (event.ctrlKey || event.metaKey)  {
                await this.#handleModifiedSaveAction(event, actor, actionId)
            } else {
                await actor.rollSavingThrow(actionId, {})
            }
        }

        /**
         * Handle modified save action
         * @private
         * @param {object} event    The event
         * @param {object} actor    The actor
         * @param {string} actionId The action id
         */
        async #handleModifiedSaveAction(event, actor, actionId) {
            await actor.rollSavingThrow(actionId, {showModifierDialog: true})
        }


        /**
         * Handle ability action
         * @private
         * @param {object} event    The event
         * @param {object} actor    The actor
         * @param {string} actionId The action id
         */
        #handleAbilityAction(event, actor, actionId) {
            if (event.ctrlKey || event.metaKey)  {
                this.#handleModifiedAbilityAction(event, actor, actionId)
            } else {
                actor.rollAbilityCheck(actionId, {})
            }
        }

        /**
         * Handle modified ability action
         * @private
         * @param {object} event    The event
         * @param {object} actor    The actor
         * @param {string} actionId The action id
         */
        #handleModifiedAbilityAction(event, actor, actionId) {
            actor.rollAbilityCheck(actionId, {showModifierDialog: true})
        }

        /**
         * Handle Class Action
         * @param event
         * @param actor
         * @param actionId
         */
        #handleClassAction(event, actor, actionId) {
            if(actionId === 'deed') {
                this.actor.rollAttackBonus();
                return;
            }
            if (actionId === 'spellCheck') {
                this.actor.rollSpellCheck();
                return;
            }
            if (actionId === 'luck_die') {
                this.actor.rollLuckDie();
                return;
            }
            if (actionId.startsWith('skill_')) {
                const skillName = actionId.replace('skill_','');
                this.actor.rollSkillCheck(skillName);
            }
        }

        #handleClassModifiedAction(event, actor, actionId) {
            if(actionId === 'deed') {
                this.actor.rollAttackBonus({showModifierDialog: true});
                return;
            }
            if (actionId === 'spellCheck') {
                this.actor.rollSpellCheck({showModifierDialog: true});
                return;
            }
            if (actionId === 'luck_die') {
                this.actor.rollLuckDie({showModifierDialog: true})
                return;
            }
            if (actionId.startsWith('skill_')) {
                const skillName = actionId.replace('skill_','');
                this.actor.rollSkillCheck(skillName, {showModifierDialog: true});
            }
        }

        /**
         * Handle utility action
         * @private
         * @param {object} token    The token
         * @param {string} actionId The action id
         */
        async #handleUtilityAction(token, actionId) {
            switch (actionId) {
                case 'endTurn':
                    if (game.combat?.current?.tokenId === token.id) {
                        await game.combat?.nextTurn()
                    }
                    break
            }
        }
    }
})
