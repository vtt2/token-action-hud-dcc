/**
 * Default layout and groups
 */
export let DEFAULTS = null
let spellGroupName = '';



Hooks.once('tokenActionHudCoreApiReady', async (coreModule) => {
    DEFAULTS = {
        layout: [
            {
                nestId: 'weapons',
                id: 'weapons',
                name: coreModule.api.Utils.i18n('DCC.ItemTypeWeaponPl'),
                groups: [
                    {
                        nestId: 'weapons_weapons',
                        id: 'weapons',
                        name: coreModule.api.Utils.i18n('DCC.ItemTypeWeaponPl'),
                        type: 'system'
                    },
                    {
                        nestId: 'weapons_modified',
                        id: 'weapons_modified',
                        name: 'Weapons (Modified)',
                        type: 'system'
                    }
                ]
            },
            {
                nestId: 'spells',
                id: 'spells',
                name: coreModule.api.Utils.i18n('DCC.ItemTypeSpellPl'),
                settings: {
                    style: 'list'
                },
                groups: []
            },
            {
                nestId: 'class',
                id: 'class',
                name: coreModule.api.Utils.i18n('DCC.ItemTypeClass'),
                settings: {
                    style: 'list'
                },
                groups: []
            },
            {
                nestId: 'saves',
                id: 'saves',
                name: coreModule.api.Utils.i18n('DCC.ActionSave'),
                settings: {
                    style: 'list'
                },
                groups: [
                    {
                        nestId: 'saves_saves',
                        id: 'saves',
                        name: coreModule.api.Utils.i18n('DCC.ActionSave'),
                        type: 'system',
                    },
                    {
                        nestId: 'saves_modified',
                        id: 'saves_modified',
                        name: coreModule.api.Utils.i18n('DCC.ActionSave') + '(Modified)',
                        type: 'system',
                    }
                ]
            },
            {
                nestId: 'abilities',
                id: 'abilities',
                name: coreModule.api.Utils.i18n('DCC.Ability'),
                settings: {
                    style: 'list'
                },
                groups: [
                    {
                        nestId: 'abilities_abilities',
                        id: 'abilities',
                        name: 'Abilities',
                        type: 'system',
                    },
                    {
                        nestId: 'abilities_modified',
                        id: 'abilities_modified',
                        name: 'Abilities (Modified)',
                        type: 'system',
                    }
                ]

            },
            {
                nestId: 'utility',
                id: 'utility',
                name: coreModule.api.Utils.i18n('tokenActionHud.utility'),
                groups: [
                    {
                        nestId: 'utility_utility',
                        id: 'utility',
                        name: coreModule.api.Utils.i18n('tokenActionHud.utility'),
                        type: 'system'
                    }
                ]
            },
        ],
        groups: [
            {id: 'abilities_abilities', name: coreModule.api.Utils.i18n('DCC.Ability'), type: 'system'},
            {id: 'abilities_modified', name: coreModule.api.Utils.i18n('DCC.Ability'), type: 'system'},
            {id: 'weapons_weapons', name: coreModule.api.Utils.i18n('DCC.ItemTypeWeaponPl'), type: 'system'},
            {id: 'weapons_modified', name: coreModule.api.Utils.i18n('DCC.ItemTypeWeaponPl'), type: 'system'},
            {id: 'utility_utility', name: coreModule.api.Utils.i18n('tokenActionHud.utility'), type: 'system'}
        ]
    }
})
